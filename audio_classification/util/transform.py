from sklearn.preprocessing import OrdinalEncoder

genre_encoding = {
    'blues': 0,
    'classical': 1,
    'country': 2,
    'disco': 3,
    'hiphop': 4,
    'jazz': 5,
    'metal': 6,
    'pop': 7,
    'reggae': 8,
    'rock': 9
}


def encode_genre(genre):
    return genre_encoding[genre]


def decode_genre(given_encoded_genre):
    for genre, encoded_genre in genre_encoding.items():
        if given_encoded_genre == encoded_genre:
            return genre


genre_encoder = OrdinalEncoder().fit(list(genre_encoding.items()))
