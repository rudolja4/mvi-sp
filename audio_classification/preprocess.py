import os

import numpy as np
import pandas as pd
import librosa


DATASET_FOLDER = 'data'
DATASET_FILE = os.path.join(DATASET_FOLDER, 'input.mf')

DUMB_PREFIX = '/Users/sness/mirex2008/genres/'


def preprocess_sample(path, **kwargs):
    audio_file, sr = librosa.load(path)
    # y, _ = librosa.effects.trim(audio_file)
    return librosa.feature.mfcc(y=audio_file, sr=sr, **kwargs)


def preprocess(n_mfcc):
    output_folder = f'preprocessed_nmfcc_{n_mfcc}'
    output_file = os.path.join(output_folder, 'data.csv')
    similarity_file = os.path.join(output_folder, 'similarity.pyc')

    columns = ['file', 'label']
    df = pd.read_csv(DATASET_FILE, sep='\t', header=None, names=columns)

    length = len(df)
    done = 0

    output_index = list()

    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)
    else:
        print('Cached features found')
        return pd.read_csv(output_file), pd.read_pickle(similarity_file)

    for genre in df['label'].unique().tolist():
        genre_folder = os.path.join(output_folder, genre)
        if not os.path.isdir(genre_folder):
            os.mkdir(genre_folder)
    for index, row in df.iterrows():
        dumb_absolute_path = row['file']
        smart_suffix = dumb_absolute_path[len(DUMB_PREFIX):]
        smart_relative_path = os.path.join(DATASET_FOLDER, smart_suffix)
        smart_relative_output_path = os.path.join(output_folder, f'{smart_suffix[:-4]}.npy')

        output_index.append((smart_relative_output_path, row['label']))

        mfccs = preprocess_sample(smart_relative_path, n_mfcc=n_mfcc)
        np.save(smart_relative_output_path, mfccs)

        done += 1
        print(f'Preprocessing <{done}/{length}> ', end='\r')

    output_df = pd.DataFrame(output_index, columns=columns, index=None)
    output_df.to_csv(output_file, index=False)

    similarity_list = list()
    for i in range(len(output_index)):
        for j in range(i, len(output_index)):
            similarity_list.append((output_index[j][0], output_index[i][0], output_index[i][1] == output_index[j][1]))

    similarity_df = pd.DataFrame(similarity_list, columns=['file_1', 'file_2', 'label'], index=None)
    similarity_df.to_pickle(similarity_file, protocol=4)

    print('\nDone')
    return output_df, similarity_df


if __name__ == '__main__':
    preprocess(13)
