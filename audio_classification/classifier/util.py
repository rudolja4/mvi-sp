import numpy as np


def split_mfcc(mfcc, frame_length):
    hop_length = frame_length // 2
    mfcc_len = mfcc.shape[1]

    if mfcc_len < frame_length:
        raise Exception('Audio file too short')
    if mfcc_len == frame_length:
        return np.array([mfcc])

    mfcc_t = mfcc.T
    mfccs = []
    n_frames = 1 + int((mfcc_len - frame_length) // hop_length)
    for i in range(0, n_frames):
        start = i * hop_length
        end = start + frame_length
        mfccs.append(mfcc_t[start:end].T)
#     ranks = [1 / 2] + [1] * (n_frames - 1)

    rest = mfcc_len % hop_length
    if rest != 0:
        start = mfcc_len - frame_length
        mfccs.append(mfcc_t[start:].T)
#         ranks.append(rest / frame_length)

    return np.array(mfccs)