from abc import ABC, abstractmethod

from audio_classification.classifier.util import split_mfcc
from audio_classification.preprocess import preprocess_sample


class BaseClassifier(ABC):
    MFCC_LEN = 1290

    def __init__(
            self,
            **kwargs  # arguments for preprocessing
    ):
        self.preprocess_kwargs = kwargs

    # This should classify a mfcc matrix in shape (num_coefficients, MFCC_LEN=1296)
    @abstractmethod
    def classify(self, mfcc):
        pass

    def classify_file(self, path):
        return self.classify(preprocess_sample(path, **self.preprocess_kwargs))

    # can classify mfcc with variable length
    def classify_mfcc(self, mfcc):
        mfccs = split_mfcc(mfcc, self.MFCC_LEN)
        result = list(map(lambda x: self.classify(x), mfccs))
        return max(set(result), key=result.count)


    def fit(self, X, y, validation_data):
        pass


class MockClassifier(BaseClassifier):
    def classify(self, df):
        return 'jazz'
