import numpy as np

from audio_classification.dtwclassifier.distances import euclid
from audio_classification.dtwclassifier.ranges import default_range


def dtw(v1, v2, dist_fn=euclid, range_fn=default_range):
    v1len, v2len = len(v1), len(v2)
    mat_rows, mat_cols = v1len + 1, v2len + 1
    mat = np.full((mat_rows, mat_cols), np.inf)
    mat[0, 0] = 0

    for (i, j) in range_fn(v1len, v2len):

        cost = dist_fn(v1[i], v2[j])

        min_three = np.min([mat[i + 1, j], mat[i, j+1], mat[i, j]])
        mat[i + 1, j + 1] = cost + min_three

    return mat[v1len, v2len], mat[1:, 1:]
