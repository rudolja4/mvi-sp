from itertools import product


def default_range(n, m):
    return list(product(range(0, n), range(0, m)))


def sakoe_chiba_range(n, m, window_size=2):
    res = []

    for w in range(1, window_size):
        for i in range(0, min(n, m) - w):
            res.append((i + w, i))
        for i in range(0, min(n, m) - w):
            res.append((i, i + w))

    for i in range(0, min(n, m)):
        res.append((i, i))

    return res


def itakura_constraint(i, j, n, m, slope):
    return slope * i > j > m - 1 - slope * (n - i) and slope * j >= i >= n - 1 - slope * (m - j)


def itakura_range(n, m, s=2):
    res = []

    for i in range(1, n + 1):
        for j in range(1, m + 1):
            if itakura_constraint(i, j, n, m, s):
                res.append((i - 1, j - 1))

    return res
