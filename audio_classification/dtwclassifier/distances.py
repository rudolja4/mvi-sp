import numpy as np


def minkowski(v1, v2, pow_n):
    diff = np.absolute(np.subtract(v1, v2))
    return np.sum(np.power(diff, pow_n)) ** (1 / pow_n)


def euclid(v1, v2):
    return minkowski(v1, v2, pow_n=2)