from audio_classification.classifier.base_classifier import BaseClassifier
import numpy as np
import pandas as pd

from audio_classification.dtwclassifier.distances import euclid, minkowski
from audio_classification.dtwclassifier.dtw import dtw
from audio_classification.dtwclassifier.ranges import default_range, itakura_range


def normalize_matrices(A, B):
    norm_shape = min(A.shape[1], B.shape[1])
    return A[:, :norm_shape], B[:, :norm_shape]


class DTWClassifier(BaseClassifier):
    MODE_MEAN = 'MEAN'
    MODE_MIN = 'MIN'

    def __init__(self, mode=MODE_MEAN, n_compared_songs=10, dist_fn=euclid, range_fn=default_range):
        self.mode = mode
        self.n_compared_songs = n_compared_songs
        self.dist_fn = dist_fn
        self.range_fn = range_fn
        self.traindf = None

    def __get_mfcc_agg_func(self):
        if self.mode == self.MODE_MEAN:
            return lambda mfccs, grouped_df: sum(mfccs) / len(grouped_df)
        if self.mode == self.MODE_MIN:
            return lambda mfccs, grouped_df: min(mfccs)
        raise Exception("Bad mode specified.")

    def __compare_mfccs(self, A, B):
        A, B = normalize_matrices(A, B)
        res, _ = dtw(A, B, self.dist_fn, self.range_fn)
        return res

    def __aggregate_genre(self, grpdf, testdf, agg_func, n_compared_songs):
        first_ten = grpdf.head(n_compared_songs)
        mfccs = first_ten['file'].apply(lambda x: self.__compare_mfccs(np.load(x), testdf))
        return agg_func(mfccs, first_ten)

    def classify(self, mfcc):
        return self.__classify_mfcc(mfcc)

    def classify_multiple(self, testdf):
        result = testdf.iloc[:, 0].apply(lambda x: self.__classify_mfcc(np.load(x)))
        return pd.DataFrame(result)

    def __classify_mfcc(self, mfcc):
        result = self.traindf.groupby('label').apply(
            lambda x: self.__aggregate_genre(x, mfcc, self.__get_mfcc_agg_func(), self.n_compared_songs)
        )
        return result.idxmin()

    def fit(self, X, y, _=None):
        self.traindf = pd.merge(X, y, left_index=True, right_index=True)


class DTWClassifierFactory:
    @staticmethod
    def create():
        cls = DTWClassifier(mode=DTWClassifier.MODE_MEAN, range_fn=itakura_range,
                            dist_fn=lambda x, y: minkowski(x, y, 1), n_compared_songs=15)
        df = pd.read_csv('preprocessed_nmfcc_13/data.csv')
        cls.fit(df['file'], df['label'])
        return cls
