import os
from abc import ABC

import tensorflow as tf
import numpy as np
from sklearn.neighbors import KNeighborsClassifier

from audio_classification.classifier.base_classifier import BaseClassifier
from audio_classification.util.transform import decode_genre

MODEL_FOLDER = os.path.join('models', 'siamese')
MODEL_PATH = os.path.join(MODEL_FOLDER, 'model.h5')
KNN_SAMPLES_PATH = os.path.join(MODEL_FOLDER, 'data.npy')
KNN_LABELS_PATH = os.path.join(MODEL_FOLDER, 'labels.npy')


class ResNetSimilarityKnn(BaseClassifier):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        x = np.load(KNN_SAMPLES_PATH)
        y = np.load(KNN_LABELS_PATH)

        self.knn = KNeighborsClassifier(n_neighbors=12)
        self.knn.fit(x, y)

        self.embedding_model = tf.keras.models.load_model(MODEL_PATH, compile=False)

    def classify(self, mfcc):
        mfcc = np.expand_dims(mfcc, axis=0)
        mfcc = np.expand_dims(mfcc, axis=3)

        feature_vector = self.embedding_model.predict(mfcc)
        prediction = self.knn.predict(feature_vector)
        return decode_genre(prediction[0])


class ResNetSimilarityKnnFactory:
    @staticmethod
    def create():
        return ResNetSimilarityKnn()
