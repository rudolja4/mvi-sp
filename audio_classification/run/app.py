import os
from random import random

import librosa
from flask import Flask, request, flash, jsonify, send_from_directory, make_response
from werkzeug.utils import redirect, secure_filename

from audio_classification.dnn_simple_classifier.dnn_simple_classifier import DNNSimpleClassifierFactory
from audio_classification.dtwclassifier.dtwclassifier import DTWClassifierFactory
from audio_classification.resnet_classifier.resnet_classifier import ResNetClassifierFactory
from audio_classification.resnet_similarity_knn.nn_similarity import ResNetSimilarityKnnFactory
from audio_classification.run.utils import generate_soundwave

UPLOAD_FOLDER = 'upload'

app = Flask(__name__, static_url_path='', static_folder='static')

if not os.path.exists(UPLOAD_FOLDER):
    os.mkdir(UPLOAD_FOLDER)


simple_dnn_classifier = DNNSimpleClassifierFactory.create()
resnet_classifier = ResNetClassifierFactory.create()
dtw_classifier = DTWClassifierFactory.create()
vgg_classifier = ResNetClassifierFactory.create()
resnet_similarity_knn = ResNetSimilarityKnnFactory.create()


@app.route('/')
def index():
    return send_from_directory('static', 'index.html')


def _build_cors_prelight_response():
    response = make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response


@app.route('/classify', methods=['POST', 'OPTIONS'])
def classify():
    if request.method == "OPTIONS":  # CORS
        return _build_cors_prelight_response()

    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']

    filename = secure_filename(file.filename)
    folder = os.path.join(UPLOAD_FOLDER, f'c{random()}')
    filepath = os.path.join(folder, filename)

    try:
        os.mkdir(folder)
        file.save(filepath)
        audio_file, sr = librosa.load(filepath)
        mfcc = librosa.feature.mfcc(y=audio_file, sr=sr, n_mfcc=13)

        return jsonify({
            "classification": [
                {
                    'name': 'Simple DNN Classifier',
                    'genre': simple_dnn_classifier.classify_mfcc(mfcc)
                },
                {
                    'name': 'ResNet-18 Classifier',
                    'genre': resnet_classifier.classify_mfcc(mfcc)
                },
                {
                    'name': 'VGG-16 Classifier',
                    'genre': vgg_classifier.classify_mfcc(mfcc)
                },
                {
                    'name': 'ResNet-18  Embedding + KNN Classifier',
                    'genre': resnet_similarity_knn.classify_mfcc(mfcc)
                },
                {
                    'name': 'DTW classifier',
                    'genre': dtw_classifier.classify_mfcc(mfcc)
                }
            ],
            "dtw": simple_dnn_classifier.classify_mfcc(mfcc),
            "filename": file.filename,
            "soundwave": generate_soundwave(filepath)
        })
    except Exception as e:
        flash(e)
        return redirect(request.url)
    finally:
        if os.path.exists(filepath):
            os.remove(filepath)
        if os.path.exists(folder):
            os.rmdir(folder)


if __name__ == '__main__':
    app.run(port=4666)
