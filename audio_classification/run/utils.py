import base64
import io
import librosa
import librosa.display
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg

def generate_soundwave(filepath):
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)

    file, sr = librosa.load(filepath)
    librosa.display.waveplot(y=file, sr=sr, ax=axis)

    pic_IObytes = io.BytesIO()
    FigureCanvasAgg(fig).print_jpg(pic_IObytes)
    pic_IObytes.seek(0)
    pic_hash = base64.b64encode(pic_IObytes.read())

    return pic_hash.decode('ascii')