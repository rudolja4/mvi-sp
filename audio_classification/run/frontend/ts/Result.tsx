import React, {useEffect, useRef} from 'react';
import {base64tosrc} from "./utils";

interface Classification {
    name: string;
    genre: string;
}

export interface APIResponse {
    filename: string;
    soundwave: string;
    classification: Classification[];
}

interface ResultProps {
    response: APIResponse;
    file: string;
}

const Result: React.FC<ResultProps> = ({response, file}) => {
    const audio = useRef<HTMLAudioElement>();
    useEffect(() => {
        audio.current.pause();
        audio.current.load();
    }, [file])

    return (
        <div>
            <h1>{response.filename}</h1>
            <audio controls style={{'width': '100%'}} ref={audio}>
                <source src={file}/>
            </audio>
            <img src={base64tosrc(response.soundwave)} alt="Sound wave"/>
            <table className="results">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Predikovaný žánr</th>
                    </tr>
                </thead>
                <tbody>
                {response.classification.map(({name, genre}) => (
                    <tr key={name}>
                        <td>{name}</td>
                        <td>{genre}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    )
}

export default Result;