import './index.css';
import '@uppy/core/dist/style.min.css';
import '@uppy/dashboard/dist/style.css';

import {Uppy} from '@uppy/core';
import Dashboard from '@uppy/dashboard';
import XHRUpload from '@uppy/xhr-upload';
import ReactDOM from 'react-dom';
import React from 'react';
import Result, {APIResponse} from './ts/Result';
import {base64tosrc} from "./ts/utils";

const uppy = new Uppy({
    debug: true,
    allowMultipleUploads: true,
    autoProceed: true,
    restrictions: {
        allowedFileTypes: ['.wav']
    }
}).use(Dashboard, {
    inline: true,
    target: '#uploader',
    height: '100%',
    width: '100%',
    theme: 'light',
}).use(XHRUpload, {
    method: 'POST',
    endpoint: 'http://localhost:4666/classify',
    fieldName: 'file'
});

uppy.on('file-added', (file) => {
    uppy.getFiles()
        .filter(oldFile => file.id != oldFile.id)
        .forEach((oldFile) => uppy.removeFile(oldFile.id))
});

uppy.on('upload-success', (file, response) => {
    const resElement = document.getElementById('result');
    if (!resElement) {
        throw "No res element!";
    }
    const body = response.body as APIResponse;
    uppy.setFileState(file.id, {preview: base64tosrc(body.soundwave)})
    ReactDOM.render(<Result response={body} file={URL.createObjectURL(file.data)}/>, resElement);
});

uppy.on('complete', (result) => {
    const id = uppy.getFiles()[0].id;
});