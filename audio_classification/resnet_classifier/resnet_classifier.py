import os

import numpy as np
from keras import models

from audio_classification.classifier.base_classifier import BaseClassifier
from audio_classification.util.transform import decode_genre


class ResNetClassifier(BaseClassifier):

    RESNET_MODEL_PATH = os.path.join('models', 'resnet_classifier', 'model.h5')

    def __init__(self, model_path, **kwargs):
        super().__init__(**kwargs)
        self.model = models.load_model(model_path)

    def classify(self, mfcc):
        mfcc = np.expand_dims(mfcc, axis=0)
        mfcc = np.expand_dims(mfcc, axis=3)
        return decode_genre(self.model.predict(mfcc).argmax(axis=1)[0])


class ResNetClassifierFactory:
    @staticmethod
    def create():
        return ResNetClassifier(ResNetClassifier.RESNET_MODEL_PATH)
