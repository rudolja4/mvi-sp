import os

import numpy as np
from keras import models

from audio_classification.classifier.base_classifier import BaseClassifier
from audio_classification.util.transform import decode_genre


class DNNSimpleClassifier(BaseClassifier):

    SIMPLE_MODEL_PATH = os.path.join('models', 'simple_dnn', 'model.h5')

    def __init__(self, model_path, **kwargs):
        super().__init__(**kwargs)
        self.model = models.load_model(model_path)

    def classify(self, mfcc):
        mfcc = np.expand_dims(mfcc, axis=0)
        mfcc = np.expand_dims(mfcc, axis=3)
        res = self.model.predict_classes(mfcc)
        return decode_genre(res[0])


class DNNSimpleClassifierFactory:
    @staticmethod
    def create():
        return DNNSimpleClassifier(DNNSimpleClassifier.SIMPLE_MODEL_PATH)
