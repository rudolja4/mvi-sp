#!/bin/bash
if [ -d "data" -a ! -h "data" ]
then
    echo "Data are in folder ./data"
else
    curl -O http://opihi.cs.uvic.ca/sound/genres.tar.gz
    tar -xzf genres.tar.gz
    mv genres data
    rm genres.tar.gz
fi
