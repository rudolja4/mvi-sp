clean-cache:
	rm -rf preprocessed_* data

clean-build:
	rm -rf venv audio_classification.egg-info

clean-all: clean-build clean-cache

setup-dev: clean-build
	python3 -m venv venv
	./venv/bin/pip3 install -U -r requirements.txt
	./venv/bin/pip3 install -e .

venv-activate:
	. ./venv/bin/activate

get-dataset:
	. ./bin/get-dataset.sh

preprocess: get-dataset
	. ./venv/bin/activate && \
	python3 audio_classification/preprocess.py

build-frontend:
	rm -rf audio_classification/run/static
	parcel build audio_classification/run/frontend/index.html --out-dir audio_classification/run/static

watch-frontend:
	parcel audio_classification/run/frontend/index.html

export-libs:
	./venv/bin/pip3 freeze > requirements.txt

ntb:
	. ./venv/bin/activate && \
	jupyter notebook

server:
	. ./venv/bin/activate && \
	python3 audio_classification/run/app.py
