# Music genre classification
Tomáš Rokos, Jan Rudolf

----

In this project we tested several approaches to music genre classification. Details can be found in our reports:
[report - Jan Rudolf](report_rudolja4.pdf), [report - Tomáš Rokos](report_rokosto1.pdf).

A working demo can be found at [http://134.209.243.49/](http://134.209.243.49/).

# Setting up environment
## Dependencies
In order to run project you need following: python3, python3-venv, make

Ubuntu: ```sudo apt install -y python3 python3-venv make```

## Setup project
Following command will download all required libraries and create virtual environment

```make setup-dev```

All needed data are downloaded automatically when they are needed (if not cached).

Once the project is set up, you can run app using ```make server``` or browse notebooks using ```make ntb```.
